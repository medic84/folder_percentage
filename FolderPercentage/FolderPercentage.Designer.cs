﻿namespace FolderPercentage
{
    partial class FolderPercentage
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FolderPercentage));
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.hChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.hWorker = new System.ComponentModel.BackgroundWorker();
            this.hSelectFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.hStatusBar = new System.Windows.Forms.StatusStrip();
            this.hProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.hInfoLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.hFullSizeLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripSplitButton1 = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.hNewScan = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.hChart)).BeginInit();
            this.hStatusBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // hChart
            // 
            this.hChart.BackColor = System.Drawing.Color.Transparent;
            this.hChart.BorderlineColor = System.Drawing.Color.Transparent;
            chartArea1.Name = "ChartArea1";
            this.hChart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.hChart.Legends.Add(legend1);
            resources.ApplyResources(this.hChart, "hChart");
            this.hChart.Name = "hChart";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;
            series1.Legend = "Legend1";
            series1.Name = "Folders";
            this.hChart.Series.Add(series1);
            // 
            // hWorker
            // 
            this.hWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Work_DoWork);
            // 
            // hSelectFolder
            // 
            resources.ApplyResources(this.hSelectFolder, "hSelectFolder");
            this.hSelectFolder.ShowNewFolderButton = false;
            // 
            // hStatusBar
            // 
            this.hStatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hProgressBar,
            this.hInfoLabel,
            this.hFullSizeLabel,
            this.toolStripSplitButton1});
            resources.ApplyResources(this.hStatusBar, "hStatusBar");
            this.hStatusBar.Name = "hStatusBar";
            this.hStatusBar.SizingGrip = false;
            // 
            // hProgressBar
            // 
            this.hProgressBar.Name = "hProgressBar";
            resources.ApplyResources(this.hProgressBar, "hProgressBar");
            // 
            // hInfoLabel
            // 
            this.hInfoLabel.BackColor = System.Drawing.Color.Transparent;
            this.hInfoLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)));
            this.hInfoLabel.Margin = new System.Windows.Forms.Padding(5, 3, 5, 2);
            this.hInfoLabel.Name = "hInfoLabel";
            this.hInfoLabel.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            resources.ApplyResources(this.hInfoLabel, "hInfoLabel");
            this.hInfoLabel.Spring = true;
            // 
            // hFullSizeLabel
            // 
            this.hFullSizeLabel.BackColor = System.Drawing.Color.Transparent;
            this.hFullSizeLabel.Name = "hFullSizeLabel";
            resources.ApplyResources(this.hFullSizeLabel, "hFullSizeLabel");
            // 
            // toolStripSplitButton1
            // 
            this.toolStripSplitButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSplitButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox1,
            this.toolStripSeparator1,
            this.toolStripMenuItem4,
            this.toolStripMenuItem3,
            this.toolStripMenuItem2,
            this.toolStripMenuItem1});
            resources.ApplyResources(this.toolStripSplitButton1, "toolStripSplitButton1");
            this.toolStripSplitButton1.Name = "toolStripSplitButton1";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            resources.ApplyResources(this.toolStripTextBox1, "toolStripTextBox1");
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            resources.ApplyResources(this.toolStripMenuItem4, "toolStripMenuItem4");
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            resources.ApplyResources(this.toolStripMenuItem3, "toolStripMenuItem3");
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            resources.ApplyResources(this.toolStripMenuItem2, "toolStripMenuItem2");
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            resources.ApplyResources(this.toolStripMenuItem1, "toolStripMenuItem1");
            // 
            // hNewScan
            // 
            resources.ApplyResources(this.hNewScan, "hNewScan");
            this.hNewScan.Name = "hNewScan";
            this.hNewScan.UseVisualStyleBackColor = true;
            this.hNewScan.Click += new System.EventHandler(this.hNewScan_Click);
            // 
            // FolderPercentage
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.hNewScan);
            this.Controls.Add(this.hStatusBar);
            this.Controls.Add(this.hChart);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FolderPercentage";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            ((System.ComponentModel.ISupportInitialize)(this.hChart)).EndInit();
            this.hStatusBar.ResumeLayout(false);
            this.hStatusBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart hChart;
        private System.ComponentModel.BackgroundWorker hWorker;
        private System.Windows.Forms.FolderBrowserDialog hSelectFolder;
        private System.Windows.Forms.StatusStrip hStatusBar;
        private System.Windows.Forms.ToolStripProgressBar hProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel hInfoLabel;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripStatusLabel hFullSizeLabel;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.Button hNewScan;
    }
}

