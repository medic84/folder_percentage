﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace FolderPercentage
{
    public partial class FolderPercentage : Form
    {
        private double fullFolderSize = 0;

        public FolderPercentage(string[] arg)
        {
            InitializeComponent();
            if (arg.Length > 0)
            {
                this.startProcess(arg[0]);
            } else
            {
                this.startProcess(null);
            }     
        }

        private void startProcess(string folder = null)
        {
            hChart.Series[0].Points.Clear();
            if (folder == null && hSelectFolder.ShowDialog() == DialogResult.OK)
            {
                folder = hSelectFolder.SelectedPath;
            }
            
            if (folder != null && Directory.Exists(folder)) {
                this.Text = Properties.Resources.FOLDERSIZE + folder;
                hWorker.RunWorkerAsync(folder);
            } else
            {
                MessageBox.Show(
                    Properties.Resources.FOLDERNOTDEFINED,
                    Properties.Resources.WARNING,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation
                    );
                Application.Exit();
                return;
            }
        }

        private void SafeSetInfoToStatusStrip(ToolStripItem toolStripControl, string text)
        {
            if (toolStripControl.GetCurrentParent().InvokeRequired)
            {
                toolStripControl.GetCurrentParent().Invoke(
                    new Action<string>((s) => toolStripControl.Text = s),
                    text
                    );
            }
            else
            {
                toolStripControl.GetCurrentParent().Text = text;
            }
        }

        private void SafeSetInfoToStatusStrip(ToolStripProgressBar toolStripControl, int number)
        {
            if (toolStripControl.GetCurrentParent().InvokeRequired)
            {
                toolStripControl.GetCurrentParent().Invoke(
                    new Action<int>((s) => toolStripControl.Value = s),
                    number
                    );
            }
            else
            {
                toolStripControl.Value = number;
            }
        }

        private bool SetInformation(DirectoryInfo information)
        {
            this.SafeSetInfoToStatusStrip(hInfoLabel, Properties.Resources.ANALIZEFOLDER + information.Name);

            double folderSize = information.EnumerateFiles("*", SearchOption.AllDirectories).Sum(fi => fi.Length) / 1024 / 1024;
            this.fullFolderSize += Math.Round(folderSize, 2);

            if (folderSize > 100) //Megabytes
            {
                DataPoint newPoint = new DataPoint
                {
                    YValues = new double[] { folderSize },
                    Label = String.Format("{0} {1}", folderSize, Properties.Resources.MB_MOD),
                    LegendText = String.Format("{0} {1} {2}", information.Name, folderSize, Properties.Resources.MB_MOD)
                };

                hChart.Invoke(new Action<DataPoint>((p) => hChart.Series[0].Points.Add(p)), newPoint);
            }

            double folderSizeInGb = Math.Round((this.fullFolderSize / 1024), 2);
            this.SafeSetInfoToStatusStrip(hFullSizeLabel,
                String.Format("{0} {1} {2}", Properties.Resources.ALL, folderSizeInGb, Properties.Resources.GB_MOD));
            return true;
        }

        private void Work_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            double current = 0;
            string folder = (string)e.Argument;

            string[] allDirectories = Directory.GetDirectories(folder);
            foreach (string directory in allDirectories)
            {
                try
                {
                    var info = new DirectoryInfo(directory);
                    this.SetInformation(info);
                } catch (UnauthorizedAccessException)
                {
                    Console.WriteLine(Properties.Resources.FOLDERACCESSERROR + directory);
                }
                double percent = (++current / allDirectories.Length) * 100;
                this.SafeSetInfoToStatusStrip(hProgressBar, (int)percent);
            }

            this.SafeSetInfoToStatusStrip(hProgressBar, 100);
            this.SafeSetInfoToStatusStrip(hInfoLabel, Properties.Resources.DONE);
        }

        private void hNewScan_Click(object sender, EventArgs e)
        {
            this.startProcess(null);
        }
    }
}
